package com.bootdo.hostpital.dao;

import com.bootdo.hostpital.domain.CustomMainDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 客户表
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
@Mapper
public interface CustomMainDao {

	CustomMainDO get(Long id);
	
	List<CustomMainDO> list(Map<String,Object> map);
	
	int count(Map<String,Object> map);
	
	int save(CustomMainDO customMain);
	
	int update(CustomMainDO customMain);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
