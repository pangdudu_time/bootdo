package com.bootdo.hostpital.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.hostpital.dao.EmployMainDao;
import com.bootdo.hostpital.domain.EmployMainDO;
import com.bootdo.hostpital.service.EmployMainService;



@Service
public class EmployMainServiceImpl implements EmployMainService {
	@Autowired
	private EmployMainDao employMainDao;
	
	@Override
	public EmployMainDO get(Long id){
		return employMainDao.get(id);
	}
	
	@Override
	public List<EmployMainDO> list(Map<String, Object> map){
		return employMainDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return employMainDao.count(map);
	}
	
	@Override
	public int save(EmployMainDO employMain){
		return employMainDao.save(employMain);
	}
	
	@Override
	public int update(EmployMainDO employMain){
		return employMainDao.update(employMain);
	}
	
	@Override
	public int remove(Long id){
		return employMainDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return employMainDao.batchRemove(ids);
	}
	
}
