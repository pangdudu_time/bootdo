package com.bootdo.hostpital.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.hostpital.dao.CustomMainDao;
import com.bootdo.hostpital.domain.CustomMainDO;
import com.bootdo.hostpital.service.CustomMainService;



@Service
public class CustomMainServiceImpl implements CustomMainService {
	@Autowired
	private CustomMainDao customMainDao;
	
	@Override
	public CustomMainDO get(Long id){
		return customMainDao.get(id);
	}
	
	@Override
	public List<CustomMainDO> list(Map<String, Object> map){
		return customMainDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return customMainDao.count(map);
	}
	
	@Override
	public int save(CustomMainDO customMain){
		return customMainDao.save(customMain);
	}
	
	@Override
	public int update(CustomMainDO customMain){
		return customMainDao.update(customMain);
	}
	
	@Override
	public int remove(Long id){
		return customMainDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return customMainDao.batchRemove(ids);
	}
	
}
