package com.bootdo.hostpital.service;

import com.bootdo.hostpital.domain.CustomMainDO;

import java.util.List;
import java.util.Map;

/**
 * 客户表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
public interface CustomMainService {
	
	CustomMainDO get(Long id);
	
	List<CustomMainDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CustomMainDO customMain);
	
	int update(CustomMainDO customMain);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
