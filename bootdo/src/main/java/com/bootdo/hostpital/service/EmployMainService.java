package com.bootdo.hostpital.service;

import com.bootdo.hostpital.domain.EmployMainDO;

import java.util.List;
import java.util.Map;

/**
 * 员工表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
public interface EmployMainService {
	
	EmployMainDO get(Long id);
	
	List<EmployMainDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(EmployMainDO employMain);
	
	int update(EmployMainDO employMain);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
