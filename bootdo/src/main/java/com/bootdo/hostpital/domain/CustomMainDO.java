package com.bootdo.hostpital.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



/**
 * 客户表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
public class CustomMainDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//编号
	private Long id;
	//所属员工id
	private Integer employId;
	//客户名称
	private String coustomName;
	//性别
	private String sex;
	//年龄
	private Integer age;
	//手机号
	private String telephone;
	//城市
	private String address;
	//花费
	private BigDecimal cost;
	//详情
	private String note;

	/**
	 * 设置：编号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：编号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：所属员工id
	 */
	public void setEmployId(Integer employId) {
		this.employId = employId;
	}
	/**
	 * 获取：所属员工id
	 */
	public Integer getEmployId() {
		return employId;
	}
	/**
	 * 设置：客户名称
	 */
	public void setCoustomName(String coustomName) {
		this.coustomName = coustomName;
	}
	/**
	 * 获取：客户名称
	 */
	public String getCoustomName() {
		return coustomName;
	}
	/**
	 * 设置：性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * 设置：年龄
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	/**
	 * 获取：年龄
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * 设置：手机号
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * 获取：手机号
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * 设置：城市
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：城市
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：花费
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	/**
	 * 获取：花费
	 */
	public BigDecimal getCost() {
		return cost;
	}
	/**
	 * 设置：详情
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * 获取：详情
	 */
	public String getNote() {
		return note;
	}
}
