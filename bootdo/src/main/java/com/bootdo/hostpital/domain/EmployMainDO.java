package com.bootdo.hostpital.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 员工表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
public class EmployMainDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//编号
	private Long id;
	//员工名称
	private String employName;
	//性别
	private String sex;
	//年龄
	private Integer age;
	//手机号
	private String telephone;
	//详情
	private String note;
	//地址
	private String address;

	/**
	 * 设置：编号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：编号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：员工名称
	 */
	public void setEmployName(String employName) {
		this.employName = employName;
	}
	/**
	 * 获取：员工名称
	 */
	public String getEmployName() {
		return employName;
	}
	/**
	 * 设置：性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * 设置：年龄
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	/**
	 * 获取：年龄
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * 设置：手机号
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * 获取：手机号
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * 设置：详情
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * 获取：详情
	 */
	public String getNote() {
		return note;
	}
	/**
	 * 设置：地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：地址
	 */
	public String getAddress() {
		return address;
	}
}
