package com.bootdo.hostpital.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.hostpital.domain.CustomMainDO;
import com.bootdo.hostpital.service.CustomMainService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 客户表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
 
@Controller
@RequestMapping("/hostpital/customMain")
public class CustomMainController {
	@Autowired
	private CustomMainService customMainService;
	
	@GetMapping()
	@RequiresPermissions("hostpital:customMain:customMain")
	String CustomMain(){
	    return "hostpital/customMain/customMain";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("hostpital:customMain:customMain")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<CustomMainDO> customMainList = customMainService.list(query);
		int total = customMainService.count(query);
		PageUtils pageUtils = new PageUtils(customMainList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("hostpital:customMain:add")
	String add(){
	    return "hostpital/customMain/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("hostpital:customMain:edit")
	String edit(@PathVariable("id") Long id,Model model){
		CustomMainDO customMain = customMainService.get(id);
		model.addAttribute("customMain", customMain);
	    return "hostpital/customMain/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("hostpital:customMain:add")
	public R save( CustomMainDO customMain){
		if(customMainService.save(customMain)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("hostpital:customMain:edit")
	public R update( CustomMainDO customMain){
		customMainService.update(customMain);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("hostpital:customMain:remove")
	public R remove( Long id){
		if(customMainService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("hostpital:customMain:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		customMainService.batchRemove(ids);
		return R.ok();
	}
	
}
