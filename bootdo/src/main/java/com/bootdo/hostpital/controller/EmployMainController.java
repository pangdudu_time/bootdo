package com.bootdo.hostpital.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.hostpital.domain.EmployMainDO;
import com.bootdo.hostpital.service.EmployMainService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 员工表
 * 
 * @author htc
 * @email 1992lcg@163.com
 * @date 2020-08-21 17:54:38
 */
 
@Controller
@RequestMapping("/hostpital/employMain")
public class EmployMainController {
	@Autowired
	private EmployMainService employMainService;
	
	@GetMapping()
	@RequiresPermissions("hostpital:employMain:employMain")
	String EmployMain(){
	    return "hostpital/employMain/employMain";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("hostpital:employMain:employMain")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<EmployMainDO> employMainList = employMainService.list(query);
		int total = employMainService.count(query);
		PageUtils pageUtils = new PageUtils(employMainList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("hostpital:employMain:add")
	String add(){
	    return "hostpital/employMain/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("hostpital:employMain:edit")
	String edit(@PathVariable("id") Long id,Model model){
		EmployMainDO employMain = employMainService.get(id);
		model.addAttribute("employMain", employMain);
	    return "hostpital/employMain/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("hostpital:employMain:add")
	public R save( EmployMainDO employMain){
		if(employMainService.save(employMain)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("hostpital:employMain:edit")
	public R update( EmployMainDO employMain){
		employMainService.update(employMain);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("hostpital:employMain:remove")
	public R remove( Long id){
		if(employMainService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("hostpital:employMain:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		employMainService.batchRemove(ids);
		return R.ok();
	}
	
}
